import os

import pytest

from tools_mr.check_jira_reference import JIRA_ISSUE_PATTERN, get_env, main, validate_jira_references


@pytest.mark.parametrize(
    "description, expected",
    [
        ("Issue: VROOM-1234", True),  # test issue
        ("Issue: VROOM-1234, VROOM-5678", True),  # test multiple issues
        ("closes vroom-1234", True),  # test lowercase
        ("Fixes VROOM-1234", True),  # test fixes
        ("related TO: VROOM-1234", True),  # test related to and case
        ("Add new tests\nCloses VROOM-1234\nSigned-off-by: me me@redhat.com", True),  # test multiple rows
        ("Closes [VROOM-1234](https://issues.redhat.com/browse/VROOM-1234)", True),  # test link parsing
        ("No Jira reference here", False),  # test no reference
        ("Issue: VROOM-", False),  # test broken vroom
        ("VROOM-1234", False),  # test jira only
        ("Issue: vroom-xxx", False),  # test no digits
    ],
)
def test_validate_jira_references(description, expected):
    assert validate_jira_references(description) == expected


def test_failure_output(capsys):
    assert not validate_jira_references("wrong description")
    captured = capsys.readouterr()
    assert captured.out.__contains__(JIRA_ISSUE_PATTERN)
    assert captured.out.__contains__(
        "See https://gitlab.com/redhat/edge/ci-cd/pipe-x/tools/tools-mr/"
        "-/blob/main/src/tools_mr/check_jira_reference.py for the source code"
    )


def test_get_env():
    test_variable_name = "TEST_VARIABLE"
    test_variable_value = "http://example.com"
    os.environ[test_variable_name] = test_variable_value
    assert get_env(test_variable_name) == test_variable_value
    assert get_env(test_variable_name, default="X") == test_variable_value
    del os.environ[test_variable_name]
    with pytest.raises(Exception):
        assert get_env(test_variable_name)
    assert get_env(test_variable_name, required=False, default="X") == "X"


@pytest.mark.integration
def test_main():
    os.environ["GITLAB_BASE_URL"] = "https://gitlab.com"
    if os.getenv("GITLAB_TOKEN"):
        del os.environ["GITLAB_TOKEN"]
    os.environ["CI_PROJECT_ID"] = "59512584"

    # Test an MR without the expected pattern
    os.environ["CI_MERGE_REQUEST_IID"] = "3"
    with pytest.raises(SystemExit) as excinfo:
        main()
    assert excinfo.value.code == 1

    # Test an MR with the expected pattern
    os.environ["CI_MERGE_REQUEST_IID"] = "7"
    main()
