# Tools MergeRequest

A container image containing different tools used for interacting with MergeRequests.

Currently, it contains the following extra tools:

- auto-assign-mr
- check-jira-reference

## auto-assign-mr

Parses an [CODEOWNERS](https://docs.gitlab.com/ee/user/project/codeowners/) file and automatically updates the assignees and reviewer based on that information for a given MR.
It can be run by simply executing it:

```bash
$ auto-assign-mr
```

The behavior can be customized with the following environment variables:

| Name | Description | Default Value | Required? |
|---|---|---|---|
| GITLAB_BASE_URL | The base URL of the Gitlab instance | https://gitlab.cee.redhat.com | no |
| CODEOWNERS_PATH | Path to the CODEOWNERS file | .gitlab/CODEOWNERS | no |
| GITLAB_TOKEN | Gitlab Token required for authentication | --- | yes |
| CI_PROJECT_ID | ID of the project the MR is for | --- | yes |
| CI_MERGE_REQUEST_IID | ID of the MR | --- | yes |
| REPLACE_UNUSED_ASSIGNEES_WITH_GENERAL | Flag to replace unused assignees with general ones | False | no |
| REPLACE_UNUSED_REVIEWERS_WITH_GENERAL | Flag to replace unused reviewers with general ones | True | no |
| INCLUDE_MR_AUTHOR_AS_ASSIGNEE | Flag to add MR author as assignee | True | no |
| KEEP_EXISTING_ASSIGNEES_AND_REVIEWERS | Flag to keep existing assignees and reviewers | True | no |
| LABELS | Labels added to the MR | needreview | no |


## check-jira-reference

To fetch, validate and ensure all the merge requests (MRs) description contain a properly formatted Jira reference,
helping with traceability and clarity in the project’s change history.

The tool expects the MR to reference JIRA tickets using the following pattern:
```
Issue: VROOM-X, VROOM-Y, VROOM-Z
```