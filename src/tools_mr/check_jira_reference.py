#!/usr/bin/python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import re
import traceback

import gitlab
from strip_markdown import strip_markdown

JIRA_ISSUE_PATTERN = (
    r"(Issue:\s*(VROOM-\d+(, )?)+|"
    r"(?:close|closes|closed|fix|fixes|fixed|resolve|resolves|resolved|related to):?\s+VROOM-\d+)"
)


def get_env(name: str, required: bool = True, default: str | None = None) -> str:
    """Retrieve required environment variable."""
    var = os.getenv(name)
    if required and var is None:
        raise EnvironmentError(f"Required environment variable '{name}' is unset.")
    return var or default


def get_gitlab_instance(base_url: str, token: str, ssl_verify: bool = False) -> gitlab.Gitlab:
    """
    Create and return a GitLab instance.

    Args:
        base_url: The GitLab base URL.
        token (str): The GitLab private token.
        ssl_verify (bool): Verify SSL certificates.

    Returns:
        gitlab.Gitlab: The GitLab instance.
    """
    print("Creating GitLab instance")
    return gitlab.Gitlab(base_url, private_token=token, ssl_verify=ssl_verify)


def fetch_mr_description(gl: gitlab.Gitlab, project_id, mr_iid) -> str | None:
    """
    Fetch the MR description using the GitLab API.

    Args:
        gl (gitlab.Gitlab): The GitLab instance.
        project_id (str): The project ID.
        mr_iid (str): The merge request IID.

    Returns:
        str | None: The merge request description if successful, or `None` if an error occurs.
    """
    try:
        project = gl.projects.get(project_id)
        merge_request = project.mergerequests.get(mr_iid)
        print("Fetched MR description successfully.")
        return merge_request.description
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching MR description for project {project_id} and MR {mr_iid}:")
        print(e)
        traceback.print_exc()
        return None  # Return None to indicate failure


def validate_jira_references(description: str) -> bool:
    """
    Check if the description contains valid references to JIRA issues
    with supported patterns (e.g., close, closes, fixed, etc.).

    Args:
        description (str): The merge request description.

    Returns:
        bool: True if a jira reference is found, False if not.
    """
    # Needs to extract links as a text
    description = strip_markdown(description)

    if re.search(JIRA_ISSUE_PATTERN, description, re.IGNORECASE):
        print("Jira reference check passed.")
        return True
    else:
        print("Error: MR description is missing reference to a JIRA ticket.")
        print("Expected formats include:")
        print("  - Issue: VROOM-X, VROOM-Y")
        print("  - close VROOM-X")
        print("  - fixes VROOM-Y")
        print("  - resolved: VROOM-Z")
        print("  - related to VROOM-Z")
        print(f"The full set of expected patterns is {JIRA_ISSUE_PATTERN}.")
        print(
            "See https://gitlab.com/redhat/edge/ci-cd/pipe-x/tools/tools-mr/-/blob/main/src/"
            "tools_mr/check_jira_reference.py for the source code."
        )
        return False


def main():
    # Get environment variables
    CI_SERVER_URL = get_env("CI_SERVER_URL", required=False, default="https://gitlab.com")
    GITLAB_BASE_URL = get_env("GITLAB_BASE_URL", required=False, default=CI_SERVER_URL)
    GITLAB_TOKEN = get_env("GITLAB_TOKEN", required=False)
    PROJECT_ID = get_env("CI_PROJECT_ID")  # Project ID where the MR is located
    MERGE_REQUEST_IID = get_env("CI_MERGE_REQUEST_IID")  # The internal MR ID

    # Initialize GitLab instance
    gitlab_instance = get_gitlab_instance(GITLAB_BASE_URL, GITLAB_TOKEN)

    # Fetch MR description
    description = fetch_mr_description(gitlab_instance, PROJECT_ID, MERGE_REQUEST_IID)

    if description is None:
        print("Failed to fetch the MR description. Exiting.")
        exit(1)

    # Validate the description format
    if not validate_jira_references(description):
        print("Validation failed: MR description must include a reference to Jira tickets.")
        exit(1)
    else:
        print("MR description validation successful.")


if __name__ == "__main__":
    main()
