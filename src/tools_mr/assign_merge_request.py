#!/usr/bin/python3
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import fnmatch
import os
from typing import Any, List, Set, Tuple

import gitlab


def get_required_env(name: str) -> str:
    var = os.getenv(name)
    if var is None:
        raise EnvironmentError(f"Required environment variable '{name}' is missing")
    return var


# Get environment variables
GITLAB_BASE_URL = os.getenv("GITLAB_BASE_URL", "https://gitlab.cee.redhat.com")
CODEOWNERS_PATH = os.getenv("CODEOWNERS_PATH", ".gitlab/CODEOWNERS")
GITLAB_TOKEN = get_required_env(
    "GITLAB_TOKEN",
)
PROJECT_ID = get_required_env("CI_PROJECT_ID")
MERGE_REQUEST_IID = get_required_env("CI_MERGE_REQUEST_IID")

REPLACE_UNUSED_ASSIGNEES_WITH_GENERAL = os.getenv("REPLACE_UNUSED_ASSIGNEES_WITH_GENERAL", "False").lower() == "true"
REPLACE_UNUSED_REVIEWERS_WITH_GENERAL = os.getenv("REPLACE_UNUSED_REVIEWERS_WITH_GENERAL", "True").lower() == "true"
INCLUDE_MR_AUTHOR_AS_ASSIGNEE = os.getenv("INCLUDE_MR_AUTHOR_AS_ASSIGNEE", "True").lower() == "true"
KEEP_EXISTING_ASSIGNEES_AND_REVIEWERS = os.getenv("KEEP_EXISTING_ASSIGNEES_AND_REVIEWERS", "True").lower() == "true"
LABELS = os.getenv("LABELS", "needreview").split(",")


def get_gitlab_instance(token: str, ssl_verify: bool = False) -> gitlab.Gitlab:
    """
    Create and return a GitLab instance.

    Args:
        token (str): The GitLab private token.
        ssl_verify (bool): Verify SSL certificates.

    Returns:
        gitlab.Gitlab: The GitLab instance.
    """
    print("Creating GitLab instance")
    return gitlab.Gitlab(GITLAB_BASE_URL, private_token=token, ssl_verify=ssl_verify)


def get_project(gl: gitlab.Gitlab, project_id: str) -> Any:
    """
    Get the project from GitLab.

    Args:
        gl (gitlab.Gitlab): The GitLab instance.
        project_id (str): The project ID.

    Returns:
        Any: The GitLab project.
    """
    print(f"Getting project with ID: {project_id}")
    return gl.projects.get(project_id)


def get_merge_request(project: Any, mr_iid: str) -> Any:
    """
    Get the merge request from the project.

    Args:
        project (Any): The GitLab project.
        mr_iid (str): The merge request IID.

    Returns:
        Any: The merge request.
    """
    print(f"Getting merge request with IID: {mr_iid}")
    return project.mergerequests.get(mr_iid)


def read_codeowners_file(path: str) -> List[str]:
    """
    Read the CODEOWNERS file.

    Args:
        path (str): The path to the CODEOWNERS file.

    Returns:
        List[str]: The lines of the CODEOWNERS file.

    Raises:
        FileNotFoundError: If the CODEOWNERS file is not found.
    """
    print(f"Reading CODEOWNERS file from: {path}")
    if not os.path.exists(path):
        raise FileNotFoundError(f"{path} not found")
    with open(path, "r") as file:
        lines = file.readlines()
    print("CODEOWNERS file read successfully")
    return lines


def parse_codeowners(lines: List[str]) -> dict:
    """
    Parse the CODEOWNERS file lines into a structured format.

    Args:
        lines (List[str]): The lines of the CODEOWNERS file.

    Returns:
        dict: A dictionary with parsed codeowners data.
    """
    print("Parsing CODEOWNERS file")
    parsed_data = {"codeowners_rules": [], "general_reviewers": [], "wildcard_reviewers": []}

    is_general_reviewers = False

    for line in lines:
        line = line.strip()
        if not line or line.startswith("#"):
            continue
        if "General reviewers" in line and (line.startswith("[") or line.startswith("^[")):
            is_general_reviewers = True
        elif line.startswith("["):
            is_general_reviewers = False
        else:
            parts = line.split()
            pattern = parts[0]
            users = [user.lstrip("@") for user in parts[1:]]
            if is_general_reviewers:
                parsed_data["general_reviewers"].extend(users)
            elif pattern == "*":
                parsed_data["wildcard_reviewers"] = users
            else:
                parsed_data["codeowners_rules"].append((pattern, users))

    print("Parsed CODEOWNERS data: ", parsed_data)
    return parsed_data


def find_assignees_and_reviewers(
    file_path: str, codeowners_data: dict, default_assignee: str = "unused", default_reviewer: str = "unused"
) -> Tuple[List[str], List[str]]:
    """
    Find assignees and reviewers for a given file path based on CODEOWNERS.

    Args:
        file_path (str): The path of the file.
        codeowners_data (dict): The parsed codeowners data.
        default_assignee (str, optional): The default assignee if none found.
                                          Defaults to "unused".
        default_reviewer (str, optional): The default reviewer if none found.
                                          Defaults to "unused".

    Returns:
        Tuple[List[str], List[str]]: Lists of assignees and reviewers.
    """
    print(f"Finding assignees and reviewers for file: {file_path}")
    assignees = set()
    reviewers = set()
    matched_specific_pattern = False

    for pattern, users in codeowners_data["codeowners_rules"]:
        if fnmatch.fnmatch(file_path, pattern):
            assignees.update(users)
            reviewers.update(users)
            matched_specific_pattern = True

    if not matched_specific_pattern and codeowners_data["wildcard_reviewers"]:
        reviewers.update(codeowners_data["wildcard_reviewers"])

    if not matched_specific_pattern and not codeowners_data["wildcard_reviewers"]:
        reviewers.update(codeowners_data["general_reviewers"])

    if not assignees:
        assignees.add(default_assignee)

    if not reviewers:
        reviewers.add(default_reviewer)

    if REPLACE_UNUSED_ASSIGNEES_WITH_GENERAL and "unused" in assignees:
        assignees.remove("unused")
        assignees.update(codeowners_data["general_reviewers"])

    if REPLACE_UNUSED_REVIEWERS_WITH_GENERAL and "unused" in reviewers:
        reviewers.remove("unused")
        reviewers.update(codeowners_data["general_reviewers"])

    print(f"Assignees: {assignees}, Reviewers: {reviewers}")
    return list(assignees), list(reviewers)


def get_changed_files(mr: Any) -> List[str]:
    """
    Get the list of changed files in the merge request.

    Args:
        mr (Any): The merge request.

    Returns:
        List[str]: List of changed file paths.
    """
    print("Getting list of changed files in the merge request")
    changed_files = [change["new_path"] for change in mr.changes()["changes"]]
    print("Changed files: ", changed_files)
    return changed_files


def get_user_ids(gl: gitlab.Gitlab, usernames: Set[str]) -> List[int]:
    """
    Get the user IDs for a list of usernames.

    Args:
        gl (gitlab.Gitlab): The GitLab instance.
        usernames (Set[str]): Set of usernames.

    Returns:
        List[int]: List of user IDs.
    """
    print(f"Getting user IDs for usernames: {usernames}")
    ids = []
    for username in usernames:
        users = gl.users.list(username=username)
        if users:
            ids.append(users[0].id)
    print(f"User IDs: {ids}")
    return ids


def main():
    """
    Main function to process the merge request and assign reviewers and
    assignees based on CODEOWNERS.
    """
    print("Starting main function")
    gl = get_gitlab_instance(GITLAB_TOKEN)
    project = get_project(gl, PROJECT_ID)
    merge_request = get_merge_request(project, MERGE_REQUEST_IID)

    codeowners_lines = read_codeowners_file(CODEOWNERS_PATH)
    codeowners_data = parse_codeowners(codeowners_lines)

    mr_author_username = merge_request.author["username"]
    print(f"MR author username: {mr_author_username}")

    assignees_set = set()
    reviewers_set = set()

    for file_path in get_changed_files(merge_request):
        assignees, reviewers = find_assignees_and_reviewers(file_path, codeowners_data)
        assignees_set.update(assignees)
        reviewers_set.update(reviewers)

    if INCLUDE_MR_AUTHOR_AS_ASSIGNEE:
        assignees_set.add(mr_author_username)

    if REPLACE_UNUSED_ASSIGNEES_WITH_GENERAL and ("unused" in assignees_set or not assignees_set):
        assignees_set.discard("unused")
        assignees_set.update(codeowners_data["general_reviewers"])

    if REPLACE_UNUSED_REVIEWERS_WITH_GENERAL and ("unused" in reviewers_set or not reviewers_set):
        reviewers_set.discard("unused")
        reviewers_set.update(codeowners_data["general_reviewers"])

    if KEEP_EXISTING_ASSIGNEES_AND_REVIEWERS:
        existing_assignees = {assignee["username"] for assignee in merge_request.assignees}
        existing_reviewers = {reviewer["username"] for reviewer in merge_request.reviewers}
        assignees_set.update(existing_assignees)
        reviewers_set.update(existing_reviewers)

    print(f"Final Assignees: {assignees_set}, Final Reviewers: {reviewers_set}")

    assignee_ids = get_user_ids(gl, assignees_set)
    reviewer_ids = get_user_ids(gl, reviewers_set)

    print(f"Assignee IDs: {assignee_ids}, Reviewer IDs: {reviewer_ids}")

    merge_request.assignee_ids = assignee_ids
    merge_request.reviewer_ids = reviewer_ids
    merge_request.labels += LABELS
    merge_request.save()
    print("Merge request updated successfully.")


if __name__ == "__main__":
    main()
